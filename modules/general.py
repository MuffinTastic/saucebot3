# Saucebot 3.0 by MuffinTastic # 2015
# Example module

import events

MODULENAME = "General"
MODULEDESC = "General module"

class general(protocol):
	@events.command("help", "Display all commands", "[command / 'long']")
	def help(self, args):
		if len(args["splitmsg"]) == 1:
			irc.send(args["channel"], "Displaying short command list. ::help long for descriptions!")
			irc.send(args["channel"], "-  " + ", ".join(events.event_command.keys()))
		elif args["splitmsg"][1] == "long":
			irc.send(args["channel"], "Displaying long command list.")
			for cmd, desc in events.event_command.iteritems():
				usage = " "+desc[2] if desc[2] != None else ""
				irc.send(args["channel"], "-  {0}{1}: {2}".format(cmd, usage, desc[1]))
		elif args["splitmsg"][1] in events.event_command:
			cmdarg = args["splitmsg"][1]
			desc = events.event_command[cmdarg]
			usage = " "+desc[2] if desc[2] != None else ""
			irc.send(args["channel"], "Displaying help for command {0}.".format(cmdarg))
			irc.send(args["channel"], "-  {0}{1}: {2}".format(cmdarg, usage, desc[1]))
		else:
			irc.send(args["channel"], "Unknown argument '{0}'. Supported: 'long'".format(args["splitmsg"][1]))
	
	@events.command("modules")
	def modules(self, args):
		if len(args["splitmsg"]) == 1:
			irc.send(args["channel"], "Loaded modules:")
			for module, properties in _globals["loaded_modules"].iteritems():
				irc.send(args["channel"], "-  {0}: {1}".format(module, properties[0]))
				irc.send(args["channel"], "---  {0}".format(', '.join(properties[1])))
		elif args["splitmsg"][1] == "long":
			if len(args["splitmsg"]) > 2:
				if args["splitmsg"][2] in _globals["loaded_modules"]:
					module, properties = args["splitmsg"][2], _globals["loaded_modules"][args["splitmsg"][2]]
					irc.send(args["channel"], "Module {0}:".format(module))
					irc.send(args["channel"], "-  {0}: {1}".format(module, properties[0]))
					for command in properties[1]:
						cmdprop = events.event_command[command]
						usage = " "+cmdprop[2] if cmdprop[2] != None else ""
						irc.send(args["channel"], "---  {0}{1}: {2}".format(command, usage, cmdprop[1]))
				else:
					irc.send(args["channel"], "Module {0} does not exist.".format(args["splitmsg"][2]))
			else:
				irc.send(args["channel"], "Loaded modules:")
				for module, properties in _globals["loaded_modules"].iteritems():
					irc.send(args["channel"], "-  {0}: {1}".format(module, properties[0]))
					for command in properties[1]:
						cmdprop = events.event_command[command]
						usage = " "+cmdprop[2] if cmdprop[2] != None else ""
						irc.send(args["channel"], "---  {0}{1}: {2}".format(command, usage, cmdprop[1]))
		elif args["splitmsg"][1] in _globals["loaded_modules"]:
			module, properties = args["splitmsg"][1], _globals["loaded_modules"][args["splitmsg"][1]]
			irc.send(args["channel"], "Module {0}:".format(module))
			irc.send(args["channel"], "-  {0}: {1}".format(module, properties[0]))
			irc.send(args["channel"], "---  {0}".format(', '.join(properties[1])))
		else:
			irc.send(args["channel"], "Module {0} does not exist.".format(args["splitmsg"][1]))
				
	@events.command("reload", "Reload modules")
	def reload(self, args):
		if args["user"] not in irc.op[args["channel"]]:
			return
	
		errors = _globals["loadmodules"](from_module = True)
		for error in errors:
			irc.send(args["channel"], error)
			
		irc.send(args["channel"], "Reloaded modules!")
		
	@events.command("eval", "dangerous function")
	def eval_cmd(self, args):
		if args["user"] not in irc.op[args["channel"]]:
			return
			
		errormsg = "Error running eval: {0}"
		
		if len(args["splitmsg"]) > 1:
			try:
				exec compile(' '.join(args["splitmsg"][1:]), '<string>', 'exec')
			except Exception, e:
				irc.send(args["channel"], errormsg.format(str(e)))
		else:
			irc.send(args["channel"], errormsg.format("requires 1+ arguments"))