# Saucebot 3.0 by MuffinTastic # 2015
# Events
import inspect

#events lists/dicts
event_startup	= []
event_shutdown	= []

event_command	= {}
event_message	= []
event_quit		= []
event_join		= []
event_leave		= []
event_kick		= []
event_nick		= []
event_mode		= []

#events decorators
def startup(func):
	event_startup.append(func)
	return func
	
def shutdown(func):
	event_shutdown.append(func)
	return func

def command(name, desc = None, usage = None):
	if desc == None:
		desc = "This command has no description"
	
	def deco(func):
		func.module = inspect.getfile(func)
		event_command[name] = (func, desc, usage)
		return func

	return deco

def message(func):
	event_message.append(func)
	return func
	
def nick(func):
	event_nick.append(func)
	return func
	
def join(func):
	event_join.append(func)
	return func

def leave(func):
	event_leave.append(func)
	return func
	
def kick(func):
	event_kick.append(func)
	return func
	
def quit(func):
	event_quit.append(func)
	return func
	
def mode(func):
	event_mode.append(func)
	return func