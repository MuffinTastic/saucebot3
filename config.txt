{
  "nickname": "Saucebot",
  "description": "Saucebot V3",
  "prefix": "::",

  "server": "irc.quakenet.org",
  "port": "6667",
  
  "channels": {
	"#saucebot3": null
  },

  "modules": [
    "general",
	"something"
  ],
  
  "simple": false
}
