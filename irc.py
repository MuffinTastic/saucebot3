import socket, re, math, time
from threading import Thread

# IRC connection wrapper class
class IRC:
	socket = None
	socketOpen = True
	server = None
	port   = None
	
	nickname = None
	descript = None
	channels = None
	prefix = None
	
	op 		= {}
	voice	= {}
	channel = {}
	
	buffer = ""
	msgbuffer = {}
	mbuffthread = None
	
	_cols = {
		'white':		('','00'),	
		'black':		('','01'),
		'dblue':		('','02'),
		'dgreen':		('','03'),
		'red':			('','04'),
		'brown':		('','05'),
		'magenta':		('','06'),
		'orange':		('','07'),
		'yellow':		('','08'),
		'green':		('','09'),
		'cyan':			('','10'),
		'lblue':		('','11'),
		'blue':			('','12'),
		'pink':			('','13'),
		'grey':			('','14'),
		'lgrey':		('','15'),
		'bold':			('',),
		'italic':		('',),
		'underline':	('',),
	}
	
	def __init__(self, config = None):
		self.server = config["server"]
		self.port = config["port"]
		
		print "Connecting to IRC server {0}:{1}..".format(self.server, str(self.port))
		
		self.nickname = config["nickname"]
		self.descript = config["description"]
		self.channels = config["channels"]
		self.prefix = config["prefix"]
		
		self.simple = config["simple"]
	
		self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.socket.connect((self.server, self.port))
		
		self.sendraw("USER {0} {0} {0} :{1}".format(self.nickname, self.descript))
		self.sendraw("NICK {0}".format(self.nickname))
		
		self.mbuffthread = Thread(target = self.msgbuffer_process)
		self.mbuffthread.start()
	
	def msgbuffer_process(self):
		lastprocess = math.floor(time.time())
		while self.socketOpen:
			curtime = math.floor(time.time())
			if curtime > lastprocess and self.msgbuffer:
				lastprocess = curtime
				for dest, msgs in [x for x in self.msgbuffer.iteritems()]:
					if msgs:
						mesg = msgs[0]
						del msgs[0]
						self.sendraw("PRIVMSG {0} :{1}".format(dest, mesg))
					else:
						del self.msgbuffer[dest]
	
	def process(self, line):
		if not self.simple:
			print "->| {0}".format(line)
		
		out = {}
		event = line.split(' ')[1]
		
		cnt = line.split(' ')
		
		if cnt[0] == "PING":
			self.sendraw("PONG {0}".format(cnt[1][1:]))
		
		if re.search(':{0}(.*) MODE {0}(.*)'.format(self.nickname), line) != None:
			for channel in self.channels.iteritems():
				self.join(*channel)
		
		chan = out["channel"] = cnt[2] if 2 < len(cnt) else None
		hostname = cnt[0][1:].split('!')
		if len(hostname) == 1:
			hostname = [hostname[0], hostname[0]]
		else:
			hostname = [hostname[0], hostname[1].split("@")[1]]
		usr = cnt[0][1:].split('!')[0]
		
		if event == "PRIVMSG" or event == "PART":
			out["user"], out["host"] = hostname
			out["message"] = ' '.join(cnt[3:])[1:]
			out["splitmsg"] = [x for x in out["message"].split(" ") if x]
			
			if event == "PART":
				if out["user"] in self.op[chan]:
					self.op[chan].remove(out["user"])
				if out["user"] in self.voice[chan]:
					self.voice[chan].remove(out["user"])
					
				self.channel[chan].remove(out["user"])
		if event == "JOIN":
			out["user"], out["host"] = hostname
			if usr != self.nickname:
				self.channel[chan].append(out["user"])
		if event == "KICK":
			out["kicker"], out["host"] = hostname
			out["user"] = cnt[3]
			out["message"] = ' '.join(cnt[4:])[1:]
			out["splitmsg"] = [x for x in out["message"].split(" ") if x]
			
			if out["user"] in self.op[chan]:
				self.op[chan].remove(out["user"])
			if out["user"] in self.voice[chan]:
				self.voice[chan].remove(out["user"])
			
			self.channel[chan].remove(out["user"])
			
		if event == "QUIT":
			out["user"], out["host"] = hostname
			out["message"] = ' '.join(cnt[2:])[1:]
			out["splitmsg"] = [x for x in out["message"].split(" ") if x]
			for chan in self.op:
				if out["user"] in self.op[chan]:
					self.op[chan].remove(out["user"])
			for chan in self.voice:
				if out["user"] in self.voice[chan]:
					self.voice[chan].remove(out["user"])
			for chan in self.channel:
				if out["user"] in self.channel[chan]:
					self.channel[chan].remove(out["user"])
		if event == "NICK":
			out["user"], out["host"] = cnt[0][1:].split('!')
			out["new"] = ' '.join(cnt[2:])[1:]
			del out["channel"]
			
			for chan in self.op:
				if out["user"] in self.op[chan]:
					self.op[chan].append(out["new"])
					self.op[chan].remove(out["user"])
			for chan in self.voice:
				if out["user"] in self.voice[chan]:
					self.voice[chan].append(out["new"])
					self.voice[chan].remove(out["user"])
				
		if event == "MODE":
			out["setter"], out["host"] = hostname
			out["user"] = chan if chan == usr else cnt[4]
			out["mode"] = cnt[3]
			
			if chan != usr:
				mode = cnt[3].startswith('+')
				if cnt[3].endswith('o'):
					if mode:
						self.op[chan].append(out["user"])
					else:
						self.op[chan].remove(out["user"])
				else:
					if mode:	
						self.voice[chan].append(out["user"])
					else:
						self.voice[chan].remove(out["user"])
			
		try:
			if out["channel"] == self.nickname:
				out["channel"] = out["user"]
		except KeyError:
			pass
			
		if event == "353" and chan == self.nickname:
			__users = ' '.join(cnt[3:])[2:].split(" :")
			__chan = __users[0]
			if __chan not in self.op:
				self.op[__chan] 		= []
				self.voice[__chan] 		= []
				self.channel[__chan] 	= []
			for nick in __users[1].split(' '):
				chan_nick = nick[1:] if nick[0] in "+@" else nick
				self.channel[__chan].append(chan_nick)
				
				if nick[0] == "@":
					self.op[__chan].append(nick[1:])
				if nick[0] == "+":
					self.voice[__chan].append(nick[1:])
		
		return out, event
		
	def read(self):
		while True:
			char = self.socket.recv(1)
			if not char:
				self.socketOpen = False
				break
			if len(self.buffer) > 256:
				self.buffer = self.buffer[8:]
			self.buffer += char
			if '\n' in char:
				break
				
		return self.buffer.splitlines()[-1]
		
	def send(self, destination, message):
		try:
			self.msgbuffer[destination].append(message)
		except KeyError:
			self.msgbuffer[destination] = []
			self.msgbuffer[destination].append(message)
			
		if self.simple:
			print "{0}]{1}> {2}".format(destination, self.nickname, str(message.strip()))
		
	def sendraw(self, string):
		self.socket.send(string + "\n")
		if not self.simple:
			print "<-| {0}".format(string)
		
	def leave(self, channel, reason = ''):
		self.sendraw("PART {0} :{1}".format(channel.split(' ')[0], reason))
		if self.simple:
			print "{0} << {1}".format(self.nickname, channel)
		
	def join(self, channel, password = ''):
		self.sendraw("JOIN {0} {1}".format(channel, password))
		if self.simple:
			print "{0} >> {1}".format(self.nickname, channel)
		
	def quit(self, message):
		self.sendraw("QUIT :{0}".format(message))
		if self.simple:
			print "{0} << [{1}]".format(self.nickname, message)
		self.socket.shutdown(socket.SHUT_RDWR)
		self.socket.close()
		self.socketOpen = False
		
	def nick(self, newnick):
		self.sendraw("NICK {0}".format(newnick))
		if self.simple:
			print "{0} +> {1}".format(self.nickname, newnick)
			
	def color(self, text, cols):
		if type(cols) != list:
			cols = [cols]
		prefix = []
		suffix = []
		for col in cols:
			if col in self._cols.keys():
				addp = ""
				sym = self._cols[col]
				addp += sym[0]
				if len(sym) > 1:
					addp += sym[1]
				prefix.append(addp)
				suffix.append(sym[0])
			else:
				raise KeyError("No such color: {0}".format(col))
		return ''.join(prefix) + text + ''.join(suffix[::-1]) + ''

class Protocol:
	pass