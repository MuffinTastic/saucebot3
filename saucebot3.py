# Saucebot 3.0 by MuffinTastic # 2015
# Main

import traceback, os, json, re, events, irc

config = {}
def loadconfig():
	jsonfile = json.load(open("config.txt", 'rb'))
	config["server"]		= jsonfile.get("server", "irc.quakenet.org")
	config["port"]			= int(jsonfile.get("port", "6667"))
	config["channels"]		= dict({x if x[1] != None else (x[0], '') for x in jsonfile.get("channels", {"#saucebot3", ''}).iteritems() if x[0][0] == '#'})
	config["nickname"]		= jsonfile.get("nickname", "Saucebot")
	config["description"]	= jsonfile.get("description", "Saucebot")
	config["modules"]		= jsonfile.get("modules", [])
	config["prefix"]		= jsonfile.get("prefix", "::")
	config["simple"]		= jsonfile.get("simple", True)
	
loadconfig()
	
curIRC = irc.IRC(config)
protocol = irc.Protocol()

loaded_modules = {}
def loadmodules(from_module = False):
	global protocol
	global loaded_modules
	errors = []
	
	oldconfig = {}
	for k, v in config.iteritems():
		oldconfig[k] = v
	
	loadconfig()

	loadable_modules = [file for file in os.listdir(os.getcwd()+"/modules") if file.endswith(".py")]
	loaderror = "Could not load module {0}: {1}"
	CurrentPrtcl = irc.Protocol
	
	events.event_startup	= []
	events.event_shutdown	= []
	
	events.event_command	= {}
	events.event_message	= []
	events.event_quit		= []
	events.event_join		= []
	events.event_leave		= []
	events.event_nick		= []
	
	for module in loaded_modules.values():
		del module[3]
	loaded_modules = {}
	
	for module in config["modules"]:
		module = str(module)
		if not module.endswith(".py"):
			module += ".py"
			
		if module not in loadable_modules:
			print loaderror.format(module, "module does not exist!")
		else:
			try:
				_namespace = {}
				_namespace["MODULENAME"]		= module
				_namespace["MODULEDESC"]		= "(No description)"
				_namespace["protocol"]			= CurrentPrtcl
				_namespace["irc"]				= curIRC
				_namespace["config"]			= config
				_namespace["_globals"]			= globals()
				
				execfile("modules/{0}".format(module), _namespace)
				
				CurrentPrtcl = _namespace[module[:-3]]
				
				loaded_modules[_namespace["MODULENAME"]] = [_namespace["MODULEDESC"], [], module, _namespace]
					
			except Exception, error:
				print loaderror.format(module, "\n" + traceback.format_exc())
				errors.append(loaderror.format(module, str(error)))
	
	
	curIRC.config = config
	protocol = CurrentPrtcl()
	
	for command, cmdprops in events.event_command.iteritems():
		for module, modprops in loaded_modules.iteritems():
			if modprops[2] in cmdprops[0].module:
				loaded_modules[module][1].append(command)
	
	for channel in oldconfig["channels"]:
		if channel not in config["channels"]:
			curIRC.leave(channel)
	
	for channel in config["channels"].iteritems():
		if channel not in oldconfig["channels"].iteritems():
			curIRC.join(*channel)
			
	if oldconfig["nickname"] != config["nickname"]:
		curIRC.nick(config["nickname"])
		
	if from_module:
		return errors
loadmodules()

for method in events.event_startup:
	method(protocol)

while curIRC.socketOpen:
	try:
		line = curIRC.read()
		info, event = curIRC.process(line)
		if event == "PRIVMSG":
			if info["splitmsg"][0].startswith(config["prefix"]):
				if info["splitmsg"][0][2:] in events.event_command.keys():
					events.event_command[info["splitmsg"][0][2:]][0](protocol, info)
			for method in events.event_message:
				method(protocol, info)
				
			if config["simple"]:
				print "{0}]{1}> {2}".format(info["channel"], info["user"], info["message"])
		
		if event == "JOIN":
			for method in events.event_join:
				method(protocol, info)
			
			if config["simple"]:
				print "{0} >> {1}".format(info["user"], info["channel"])
			
		if event == "PART":
			for method in events.event_leave:
				method(protocol, info)
			
			if config["simple"]:
				print "{0} << {1}".format(info["user"], info["channel"])
			
		if event == "KICK":
			for method in events.event_kick:
				method(protocol, info)
			
			if config["simple"]:
				print "{0} !> {1} [{2}]".format(info["kicker"], info["user"], info["message"])
			
		if event == "QUIT":
			for method in events.event_quit:
				method(protocol, info)
				
			if config["simple"]:
				print "{0} << [{1}]".format(info["user"], info["message"])
				
		if event == "NICK":
			for method in events.event_nick:
				method(protocol, info)
				
			if config["simple"]:
				print "{0} => {1}".format(info["user"], info["new"])
				
		if event == "MODE":
			for method in events.event_mode:
				method(protocol, info)
				
			if config["simple"]:
				mode = "+" if info["mode"].startswith("+") else "-"
				print "{0} {1}> {2}".format(info["setter"], info["mode"], info["user"])
					

	except KeyboardInterrupt:
		print "\nShutting down.."
		curIRC.quit("Shutting down")
		break
	except Exception, error:
		print(traceback.format_exc())
		if "[Errno 104] Connection reset by peer" in str(error):
			curIRC = irc.IRC(config)
		
for method in events.event_shutdown:
	method(protocol)